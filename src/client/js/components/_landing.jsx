import React, { Component } from 'react'
import Simulator from './_simulator'

export default class Landing extends Component {
  constructor(props){
    super(props)
    this.state = {totalAmount: 0}
  }

  componentDidMount(){

    $('.element').typed({
      strings: ['DÓLARES', 'EUROS', 'LIBRAS', 'YENS', 'ORO', 'PLATA'],
      typeSpeed: 160,
      backDelay: 900,
      loop: true
    })

    particlesJS("particles-js", {"particles":{"number":{"value":20,"density":{"enable":true,"value_area":200}},"color":{"value":"#fff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"","width":100,"height":140}},"opacity":{"value":.7,"random":false,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":3,"random":true,"anim":{"enable":false,"speed":40,"size_min":0.1,"sync":false}},"line_linked":{"enable":true,"distance":150,"color":"#fff","opacity":0.4,"width":1},"move":{"enable":true,"speed":1,"direction":"none","random":false,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"repulse"},"onclick":{"enable":false,"mode":"push"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":400,"size":40,"duration":2,"opacity":8,"speed":1},"repulse":{"distance":200,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":false});
    particlesJS("parti", {"particles":{"number":{"value":20,"density":{"enable":true,"value_area":200}},"color":{"value":"#fff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"","width":100,"height":400}},"opacity":{"value":.7,"random":false,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":3,"random":true,"anim":{"enable":false,"speed":40,"size_min":0.1,"sync":false}},"line_linked":{"enable":true,"distance":150,"color":"#fff","opacity":0.4,"width":1},"move":{"enable":true,"speed":1,"direction":"none","random":false,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"repulse"},"onclick":{"enable":false,"mode":"push"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":400,"size":40,"duration":2,"opacity":8,"speed":1},"repulse":{"distance":200,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":false});

  }


  render(){
    return <div className="Container_Landing">

        <section className="Landing">
          <div className="Landing__bgAbove"></div>
          <header className="LandingHeader">
            <div className="LandingHeader__logo">Genttly</div>
            <div className="LandingHeader__cont">
              <div className="LandingHeader__contact"> Whatssap: (057) 3167656743 </div>
              <div className="LandingHeader__sigIn"><a href="#">INGRESAR</a></div>
            </div>
          </header>

          <section className="Above">
            <div className="Above__container">
              <div className="Above__title">
                <div className="Above__lines">
                  <img src="dist/images/above-line1.png" alt=""/>
                  <img src="dist/images/above-line2.png" alt=""/>
                </div>
                <h1>INVIERTE EN <span className="element">EUROS</span></h1>
              </div>

              <div className="Above__register">
                <h2>APRENDE A INVERTIR AHORA</h2>
                <div className="Above__form">
                  <div className="Above__fullname">

                    <label htmlFor="">Nombre</label>
                    <input type="text" id="LandingFormName"/>
                  </div>
                  <div className="Above__cellphone">

                    <label htmlFor="">Celular</label>
                    <input type="number" id="LandingFormPhone"/>
                  </div>
                  <div className="Above__email">

                    <label htmlFor="">Correo</label>
                    <input type="text" id="LandingFormEmail"/>
                  </div>
                  <div className="Above__button">
                    <button className="waves-effect" id="LandingFormButton">ENVIAR</button>
                    <p className="errorFormMsg">Debes llenar todos los campos</p>
                  </div>
                </div>
              </div>
            </div>
            <div id="particles-js"></div>
          </section>

          <section className="Benefits">
            <div className="Benefits__container">
              <div className="Benefits__title">
                <div className="Benefits__lines">
                  <img src="dist/images/benefits-lines.png" alt=""/>
                </div>
                <h3>NUESTROS BENEFICIOS:</h3>
              </div>
              <div className="Benefits__features">
                <div>
                  <img src="dist/images/puno-puno.svg" alt=""/>
                  <h4>Asesoria para tus inversiones</h4>
                  <p>Entendemos que es necesaria una formación mental y emocional que complemente la perspectiva de cada uno de nuestros estudiantes</p>
                </div>
                <div>
                  <img src="dist/images/tiro-al-blanco.svg" alt=""/>
                  <h4>Estrategia de inversión efectivas</h4>
                  <p>Nuestro programa minimiza la perspectiva del mercado a la sencillez necesaria</p>
                </div>
                <div>
                  <img src="dist/images/bolsa-de-dinero.svg" alt=""/>
                  <h4>Aprende sin arriesgar dinero</h4>
                  <p>Tenemos simuladoes que mejoran tu rendimiento</p>
                </div>
                <div>
                  <img src="dist/images/benefits-contact.svg" alt=""/>
                  <h4>Aprende sin arriesgar dinero</h4>
                  <p>Tenemos simuladoes que mejoran tu rendimiento</p>
                </div>
                <div>
                  <img src="dist/images/benefits-social.svg" alt=""/>
                  <h4>Clases en tiempo real</h4>
                  <p>Contamos con una asistencia totalmente personalizada de +12 horas intensivas para educarte y solventar cualquier duda, en cualquier momento</p>
                </div>
              </div>
            </div>
          </section>

          <Simulator />

          <section className="LandingCourses">

            <h3>CONOCE NUESTROS CURSOS</h3>

            <div className="LandingCourses__container">
              <div className="LandingCourses__timeLineLarge">
                <span className="LandingCourses__timeLineLargeBall"></span>
              </div>
              <div className="LandingCourses__item">
                <div className="LandingCourses__day">DÍA 1
                  <span className="LandingCourses__roundLarge"><span className="LandingCourses__ballLarge"></span></span>
                  <span className="LandingCourses__dayLineLarge"></span>
                  <span className="LandingCourses__dayBallLarge"></span>
                </div>
                <div className="LandingCourses__class">
                  <div className="LandingCourses__timeLine">
                    <span className="LandingCourses__dayLine"></span>
                    <span className="LandingCourses__dayBall"></span>
                    <span className="LandingCourses__round"><span className="LandingCourses__ball"></span></span>
                  </div>
                  <h4>MOVIMIENTO DEL MERCADO</h4>
                  <p>En este nivel estudiarás maching learning y tecnologías de información</p>
                </div>
              </div>

              <div className="LandingCourses__item">
                <div className="LandingCourses__day">DÍA 2
                  <span className="LandingCourses__roundLarge"><span className="LandingCourses__ballLarge"></span></span>
                  <span className="LandingCourses__dayLineLargeDown"></span>
                  <span className="LandingCourses__dayBallLargeDown"></span>
                </div>
                <div className="LandingCourses__class">
                  <div className="LandingCourses__timeLine">
                    <span className="LandingCourses__dayLine"></span>
                    <span className="LandingCourses__dayBall"></span>
                    <span className="LandingCourses__round"><span className="LandingCourses__ball"></span></span>
                  </div>
                  <h4>MOVIMIENTO DEL MERCADO</h4>
                  <p>En este nivel estudiarás maching learning y tecnologías de información</p>
                </div>
              </div>

              <div className="LandingCourses__item">
                <div className="LandingCourses__day">DÍA 3
                  <span className="LandingCourses__roundLarge"><span className="LandingCourses__ballLarge"></span></span>
                  <span className="LandingCourses__dayLineLarge"></span>
                  <span className="LandingCourses__dayBallLarge"></span>
                </div>
                <div className="LandingCourses__class">
                  <div className="LandingCourses__timeLine">
                    <span className="LandingCourses__dayLine"></span>
                    <span className="LandingCourses__dayBall"></span>
                    <span className="LandingCourses__round"><span className="LandingCourses__ball"></span></span>
                  </div>
                  <h4>MOVIMIENTO DEL MERCADO</h4>
                  <p>En este nivel estudiarás maching learning y tecnologías de información</p>
                </div>
              </div>

              <div className="LandingCourses__item">
                <div className="LandingCourses__day">DÍA 4
                  <span className="LandingCourses__roundLarge"><span className="LandingCourses__ballLarge"></span></span>
                  <span className="LandingCourses__dayLineLargeDown"></span>
                  <span className="LandingCourses__dayBallLargeDown"></span>
                </div>
                <div className="LandingCourses__class">
                  <div className="LandingCourses__timeLine">
                    <span className="LandingCourses__dayLine"></span>
                    <span className="LandingCourses__dayBall"></span>
                    <span className="LandingCourses__round"><span className="LandingCourses__ball"></span></span>
                  </div>
                  <h4>MOVIMIENTO DEL MERCADO</h4>
                  <p>En este nivel estudiarás maching learning y tecnologías de información</p>
                </div>
              </div>

              <div className="LandingCourses__item">
                <div className="LandingCourses__day">DÍA 5
                  <span className="LandingCourses__roundLarge"><span className="LandingCourses__ballLarge"></span></span>
                  <span className="LandingCourses__dayLineLarge"></span>
                  <span className="LandingCourses__dayBallLarge"></span>
                </div>
                <div className="LandingCourses__class">
                  <div className="LandingCourses__timeLine">
                    <span className="LandingCourses__dayLine"></span>
                    <span className="LandingCourses__dayBall"></span>
                    <span className="LandingCourses__round"><span className="LandingCourses__ball"></span></span>
                  </div>
                  <h4>MOVIMIENTO DEL MERCADO</h4>
                  <p>En este nivel estudiarás maching learning y tecnologías de información</p>
                </div>
              </div>

              <div className="LandingCourses__item">
                <div className="LandingCourses__day">DÍA 6
                  <span className="LandingCourses__roundLarge"><span className="LandingCourses__ballLarge"></span></span>
                  <span className="LandingCourses__dayLineLargeDown"></span>
                  <span className="LandingCourses__dayBallLargeDown"></span>
                </div>
                <div className="LandingCourses__class">
                  <div className="LandingCourses__timeLine">
                    <span className="LandingCourses__dayLine"></span>
                    <span className="LandingCourses__dayBall"></span>
                    <span className="LandingCourses__round"><span className="LandingCourses__ball"></span></span>
                  </div>
                  <h4>MOVIMIENTO DEL MERCADO</h4>
                  <p>En este nivel estudiarás maching learning y tecnologías de información</p>
                </div>
              </div>

            </div>

          </section>

          <section className="Clients">
            <h3>NUESTROS CLIENTES</h3>
            <div className="Clients__container">
              <div id="one" className="Clients__testimonial">
                <div className="Clients__frame"></div>
                <div className="Clients__line">
                  <span></span>
                </div>
                {/* <div className="Clients__photo"><img src="dist/images/client_genttly.jpg" alt=""/></div> */}
                <div className="Clients__iframe">
                  <iframe src="https://www.youtube.com/embed/HQAPfof6UbA"></iframe>
                </div>
                <p className="Clients__testimoniaText"> <div className="Clients__quotes"></div> He logrado ganar 50.000.000 en dos meses, estos cursos son
                realmente grandiosos, nunca pensé ganar dinero tan fácil</p>
              </div>
            </div>
          </section>

          <section className="Learn">

            <div className="Learn__bg"></div>
            <div className="Learn__form">
              <h4>EMPIEZA A APRENDER AHORA</h4>
              <div className="Learn__fullname">
                <label htmlFor="">Nombre y apellido</label>
                <input type="text" id="LearnFormName"/>
              </div>
              <div className="Learn__cellphone">
                <label htmlFor="">Celular</label>
                <input id="LearnFormPhone" type="number"/>
              </div>
              <div className="Learn__email">
                <label htmlFor="">Correo</label>
                <input id="LearnFormEmail" type="text"/>
              </div>

              <div className="Learn__button">
                <button className="waves-effect" id="LearnFormButton">ENVIAR</button>
              </div>
            </div>
            <div id="parti"></div>
          </section>

          <footer className="LandingFooter">
            <div className="LandingFooter__container">
              <div><a href="#"><span className="icon icon-snapchat"></span>GENTTLY</a></div>
              <div><a href="#"><span className="icon icon-instagram"></span>GENTTLY</a></div>
              <div><a href="#"><span className="icon icon-whatsapp"></span>(057) 3167656743</a></div>
            </div>
          </footer>

        </section>

    </div>
  }
}
