import React, { Component } from 'react'

export default class ThankPage extends Component{
  render(){
    return <div className="ThankPage">
      <header className="ThankPageHeader">
        <div className="ThankPageHeader__logo">Genttly</div>
        <div className="ThankPageHeader__contact">
          <div><span className="icon icon-whatsapp"></span>(057) 3167657432</div>
          <div><a href="/login">INGRESAR</a></div>
        </div>
      </header>
      <section className="ThankPage__content">
        <div>
          <h1>GRACIAS!</h1>
          <h2>TE CONTÁCTAREMOS EN UNOS MINUTOS</h2>
          <p>ESPERA, TODAVÍA NO TE VAYAS TENEMOS UNA CLASE GRATIS PARA TI:</p>
        </div>
        <div className="video-container ThankPage__video">
          <iframe width="853" height="480" src="//www.youtube.com/embed/Q8TXgCzxEnw?rel=0" frameBorder="0" allowFullScreen></iframe>
        </div>
      </section>
      <footer className="ThankPageFooter">
        <div className="ThankPageFooter__cont">
          <div><a href="#"><span className="icon icon-snapchat"></span>GENTTLY</a></div>
          <div><a href="#"><span className="icon icon-instagram"></span>GENTTLY</a></div>
          <div><a href="#"><span className="icon icon-whatsapp"></span>(057) 3167656743</a></div>
        </div>
      </footer>
    </div>
  }
}
