from django.shortcuts import render
from rest_framework import generics
from rest_framework.permissions import AllowAny
from .models import Course, CourseClass, Lead
from .serializers import CourseSerializer, CourseClassSmallSerializer, LeadSerializer

class CourseListAPIView(generics.ListAPIView):
	model_class = Course
	queryset = Course.objects.all()
	serializer_class = CourseSerializer


class CourseRetrieveAPIView(generics.RetrieveAPIView):
	lookup_field = "slug"
	queryset = Course.objects.all()
	serializer_class = CourseSerializer
	lookup_url_kwarg = "course_slug"

class CourseClassRetrieveAPIView(generics.RetrieveAPIView):
	lookup_field = "slug"
	queryset = CourseClass.objects.all()
	serializer_class = CourseClassSmallSerializer
	lookup_url_kwarg = "courseclass_slug"

class LeadCreateAPIView(generics.CreateAPIView):
	model = Lead
	queryset = Lead.objects.all()
	serializer_class = LeadSerializer
	permission_classes = (AllowAny,)
