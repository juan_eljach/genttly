// Dependencies

import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, hashHistory } from 'react-router'

import Courses from './components/_courses'
import Classes from './components/_classes'

render((
  <Router history={hashHistory}>
    <Route path="/" component={Courses}/>
    <Route path="/classes" component={Classes}/>
  </Router>
), document.querySelector('#App'))

// -------------------- End Production

// module.hot.accept()
// require('scss')
