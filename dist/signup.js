$(function(){
  console.log('jiji');
  let $signupUsername = $('#signupUsername')
  let $signupEmail = $('#signupEmail')
  let $signupPasswordOne = $('#signupPasswordOne')
  let $signupPasswordTwo = $('#signupPasswordTwo')
  let $signupButton = $('#signupButton')

  $signupButton.click(()=> {

    let isValid = $signupUsername && $signupEmail.val() && $signupPasswordOne && $signupPasswordTwo

    if(isValid){

      $.ajax({
      url: 'http://127.0.0.1:8000/rest-auth/registration/',
      method: 'POST',
      data: {
        username: $signupUsername.val(),
        email: $signupEmail.val(),
        password1: $signupPasswordOne.val(),
        password2: $signupPasswordTwo.val()
      }
    })
    .done((data) => {
      console.log(data)
    })
    .fail((error)=> console.log(error))

    }

  })

})