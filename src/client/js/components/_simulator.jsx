import React, { Component } from 'react'

export default class Simulator extends Component {
  constructor(props){
    super(props)
    this.state = {totalAmount: 0}

    this.calcGraph = this.calcGraph.bind(this)
  }

  componentDidMount(){
    this.michart = new Chartist.Line('.ct-chart', {
      labels: [0],
      series: [[0]]
    }, {
      low: 0,
      showArea: true
    });

  }

  calcGraph(){
    var $years = parseInt($('#years').val()),
        total_months = $years * 12,
        $initial_amount = parseInt($('#amount').val()),
        monthly_percentage_return = 0.3,
        monthly_gain = $initial_amount * monthly_percentage_return,
        compounded_amount = parseInt($initial_amount)

        console.log(monthly_gain, compounded_amount)
    // l: initial count month, s: add counter month, d: divisor for height
    var dataLabels = [],
        dataSeries = [],
        l = 4,
        s = 4,
        d = 0

    // console.log(_.round(compounded_amount.toString().length))

    for(var i=0; i <= total_months; i++){

      if(compounded_amount >= 100000) {
        monthly_gain = $initial_amount * 0.1
      }

      if(i === l) {
        dataLabels.push(i)
        dataSeries.push(compounded_amount)

        l += s
      }

      compounded_amount += monthly_gain
    }

    this.michart.update({labels: dataLabels, series:[dataSeries]})

    this.setState({totalAmount: _.round(dataSeries[dataSeries.length - 1 ], 2).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")})
  }

  render(){
    return <section className="Simulator">
              <div className="Simulator__lines"><img src="dist/images/simulator-lines.png" alt=""/></div>
              <div className="Simulator__description">
                <h3>SIMULA TUS INVERSIONES</h3>
                <p>SI HACE
                <input type="text" id="years" onChange={this.calcGraph}/> AÑOS HUBIERA INVERTIDO
                <input type="text" id="amount" placeholder="$" onChange={this.calcGraph.bind(this)}/> CON EL MÉTODO DE INVERSIÓN DE GENTTLY, HOY TENDRÍA:</p>
                <div>$ { isNaN(this.state.totalAmount[this.state.totalAmount.length-1]) ? 0 : this.state.totalAmount}</div>
              </div>

              <div className="Simulator__chart">
                <span className="Simulator__amount">$ US</span>
                <div className="Simulator__chartCont">
                  <div className="ct-chart"></div>
                </div>
                <span className="Simulator__month">MES</span>
              </div>
           </section>
  }
}
