module.exports = {
  landing: 'src/client/js/landing.jsx',
  app: 'src/client/js/app.jsx',
  style: 'src/client/scss/landing.scss',
  js: 'src/client/js',
  json: 'src/client/json',
  scss: 'src/client/scss',
  dist: 'dist',
};
