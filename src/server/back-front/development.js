const express = require('express')
const path = require('path')
const webpack = require('webpack')
const config = require('../../../webpack.config.js')
const Dashboard = require('webpack-dashboard')
const DashboardPlugin = require('webpack-dashboard/plugin')
const app = express()
const compiler = webpack(config)
const dashboard = new Dashboard()

// Only for development
compiler.apply(new DashboardPlugin(dashboard.setData))
app.use(require('webpack-dev-middleware')(compiler, {
  quiet: true,
  publicPath: config.output.publicPath
}))
app.use(require('webpack-hot-middleware')(compiler, {
  log: () => {}
}))

app.use('/dist', express.static(path.resolve('./dist')))

const html = `<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/css/materialize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.10.1/chartist.min.css">
    <title>Genttly Devee</title>
  </head>
  <body>
    <div id="App" class="App"></div>
    <div class="particles-js"></div>
    <div class="parti"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/1.1.4/typed.min.js"></script>
    <script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
    <script src="https://cdn.jsdelivr.net/lodash/4.17.1/lodash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.10.1/chartist.min.js"></script>
    <script src='dist/app.js'></script>
  </body>
  </html>`

app.get('/', (req, res)=>{
  res.send(html)
})

app.listen(8080, () => console.log('localhost:8080'))
