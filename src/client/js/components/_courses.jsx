
import React, {Component} from 'react'
import Header from './_header'

export default class Courses extends Component {
  constructor(props){
    super(props)
  }

  componentDidMount(){
    $(document).ready(function(){
      $('.collapsible').collapsible();
    });

    $.getJSON('http://127.0.0.1:8000/api/courses/')
      .then((d)=>{
        console.log(d)
      })
  }

  render(){
    return <div className="CoursesView">
      <Header />
      <section className="Courses">
        <h1>CURSO INTRODUCCIÓN A FOREX</h1>
        <ul className="collapsible popout" data-collapsible="accordion">
          <li>
            <div className="collapsible-header Courses__header">
              <div className="Courses__title">MÓDULO 1 - INTRODUCCIÓN A FOREX</div>
              <div className="Courses__progress">
                <div className="Courses__progressbar">
                  <span></span>
                </div>
                <div className="Courses__percentage">80% Completado</div>
              </div>
            </div>
            <div className="collapsible-body Courses__body">
              <div className="Courses__head">
                <div>LECCIÓN</div>
                <div>DURACIÓN</div>
                <div>VISUALIZACIÓN</div>
              </div>
              <div className="Courses__class">
                <div>Origen y por qué</div>
                <div>10m 30sg</div>
                <div>o</div>
                <span>></span>
              </div>
              <div className="Courses__class">
                <div>Origen y por qué</div>
                <div>10m 30sg</div>
                <div className="Courses__visualization">o</div>
                <span>></span>
              </div>
            </div>
          </li>

        </ul>
      </section>
    </div>
  }
}
