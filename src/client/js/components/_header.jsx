import React, {Component} from 'react'

export default class Header extends Component {
  render(){
    return <header className="Header">
      <h1>Genttly</h1>
      <div className="Header__userinfo">
        <div>Juan Eljach</div>
        <img src="dist/images/Sign_in.svg" alt=""/>
      </div>
    </header>
  }
}
