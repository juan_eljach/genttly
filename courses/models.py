from django.db import models
from django.template.defaultfilters import slugify

class Course(models.Model):
    TYPES = (
        ('paid', 'Pago'),
        ('free', 'Gratis'),
    )

    name = models.CharField(max_length=140)
    description = models.TextField()
    image = models.ImageField(upload_to="uploads")
    slug = models.SlugField(max_length=300)
    available = models.BooleanField(default=True)
    course_type = models.CharField(max_length=20, choices=TYPES, default="paid")
    image_landing = models.ImageField(upload_to="uploads", null=True, blank=True)
    short_description = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Course, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

class CourseClass(models.Model):
    name = models.CharField(max_length=140)
    description = models.TextField()
    image = models.ImageField(upload_to="uploads")
    course = models.ForeignKey(Course, related_name="classes")
    slug = models.SlugField(max_length=300)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(CourseClass, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

class Lead(models.Model):
    name = models.CharField(max_length=150)
    phone = models.CharField(max_length=20)
    email = models.EmailField()
    already_contacted = models.BooleanField(default=False)