from django.contrib.auth.models import User
from django.db import models
from userena.models import UserenaBaseProfile

class UserProfile(UserenaBaseProfile):
        REQUIRED_FIELDS = ('user', 'user_type')
        user = models.OneToOneField(User, unique=True, related_name='profile')
        paid_user = models.BooleanField(default=False)
