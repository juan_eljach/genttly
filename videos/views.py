from django.shortcuts import render
from rest_framework import generics
from .models import Video, VideoComment, Answer
from .serializers import VideoSerializer, VideoCommentSerializer, AnswerSerializer

class VideoListAPIView(generics.ListAPIView):
	model_class = Video
	serializer_class = VideoSerializer
	queryset = Video.objects.all()

class VideoRetrieveAPIView(generics.RetrieveAPIView):
	queryset = Video.objects.all()
	lookup_field = "slug"
	lookup_url_kwarg = "video_slug"
	serializer_class = VideoSerializer

class VideoCommentCreateAPIView(generics.CreateAPIView):
	queryset = VideoComment.objects.all()
	serializer_class = VideoCommentSerializer

	def perfom_create(self, serializer):
		serializer.save(user_profile=self.request.user.user_profile)

class AnswerCreateAPIView(generics.CreateAPIView):
	queryset = Answer.objects.all()
	serializer_class = AnswerSerializer

	def perfom_create(self, serializer):
		serializer.save(user_profile=self.request.user.user_profile)