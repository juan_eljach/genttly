const express = require('express');
const path = require('path');
const app = express();

app.use('/dist', express.static(path.resolve('./dist')))

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'))

app.get('/', (req, res)=> res.render('index', { title: 'Genttly'}))

app.get('/thankpage', (req, res) => res.render('thankpage', { title: 'Genttly'}))

app.get('/login', (req, res)=> res.render('login', { title: 'Login'}))

app.get('/signup', (req, res)=> res.render('signup', { title: 'Signup'}))

app.get('/app', (req, res)=> res.render('app', { title: 'Genttly App' }))

app.listen(5000, ()=> console.log('localhost:5000'))
