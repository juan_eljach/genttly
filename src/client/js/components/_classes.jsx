import React, { Component } from 'react'
import Header from './_header'

export default class Classes extends Component {
  constructor(props){
    super(props)
    this.state = {isActive: 0}
  }

  componentDidMount(){
    $(document).ready(function(){
      $('.collapsible').collapsible();
    });
  }

  selectModule(m, e){
    e.preventDefault()
    if(m === this.state.isActive) this.setState({isActive:0})
    else this.setState( { isActive: m })
  }

  render(){
    return <div className="ClassesView">
      <Header className="Classes__header"/>

      <section className="Classes">
        <div className="Classes__sidebar">

          <h2 data-algo="jeje">INTRODUCCIÓN A FOREX</h2>

          <ul className="collapsible Classes__listContainer" data-collapsible="accordion">
           <li>
             <div className="collapsible-header Classes__listhead" onClick={this.selectModule.bind(this, 1)}>
             <span className={`${this.state.isActive===1 ? 'isActive': ''}`}>MÓDULO 1</span></div>
             <div className="collapsible-body Classes__listBody">
             <div className="Classes__containerList">
               <span>LECCIÓN</span>
               <ul className="Classes__lessons">
                 <div className="Classes__sidebarLine"></div>
                 <li className="isSeen">Origen y por qué<span></span></li>
                 <li>Sesiones y participantes<span></span></li>
                 <li>Volumen y volatilidad<span></span></li>
                 <li>Superposición de Mercados<span></span></li>
                 <li>Pares de divisas<span></span></li>
                 <li>Estilos de trading<span></span></li>
               </ul>
             </div>
             </div>
           </li>
          </ul>

        </div>
        <div className="Classes__container">
          <div className="Classes__nav">
            <div className="Classes__previous">
              <div>Anterior: o</div>
              <div><a href="#">Origen y por qué</a></div>
            </div>
            <div className="Classes__next">
              <div>Ir al siguiente: o</div>
              <div><a href="#">Sesiones y participantes</a></div>
            </div>
          </div>
          <div className="Classes__content">
            <div className="video-container Classes__video">
              <iframe width="853" height="480" src="//www.youtube.com/embed/Q8TXgCzxEnw?rel=0" frameBorder="0" allowFullScreen></iframe>
            </div>
            <div className="Classes__description">
              <h3>ORIGEN Y POR QUÉ</h3>
              <p>Este video contiene enseñanzas sobre finanzas y dirección para empezar a hacer inversiones efectivas </p>
            </div>
            <div className="Classes__comments">
              <div className="Classes__commentsTitle"> COMENTARIOS </div>
              <div className="Classes__commentsTextArea"><textarea name="" id="" cols="30" rows="10" maxLength="700" placeholder="Juan escribe tu comentario"></textarea></div>
              <div className="Classes__commentContainer">
                <div className="Classes__comment">
                  <div className="Classes__commentAvatar"><img src="dist/images/Sign_in.svg" alt=""/></div>
                  <div className="Classes__commentContent">
                    <p><span>David Jaimes</span> La introducción me pareció buena,
                        lástima el ruido del fondo. Yo sugiero que utilicen micrófonos en la ropa.
                    </p>
                    <div className="Classes__commentBottom">
                      <div className="Classes__commentMoment">hace 4 horas</div>
                      <div className="Classes__commentAnswer"><a href="#">responder</a></div>
                    </div>
                  </div>
                </div>
                <div className="Classes__responses">
                  <div className="Classes__response">
                    <div className="Classes__responseAvatar"><img src="dist/images/Sign_in.svg" alt=""/></div>
                    <div className="Classes__responseContent">
                      <p><span>Juan Eljach</span> Opino lo mismo</p>
                      <div className="Classes__responseMoment">hace 2 min</div>
                    </div>
                  </div>
                  <div className="Classes__response">
                    <div className="Classes__responseAvatar"><img src="dist/images/Sign_in.svg" alt=""/></div>
                    <div className="Classes__responseContent">
                      <p><span>Jessica Hernández</span> si!</p>
                      <div className="Classes__responseMoment">hace 5 min</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>
  }
}
