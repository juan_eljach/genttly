const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path')
const PATHS = require('./paths')
const root = path.resolve('./')

exports.commonDev = {
  resolve: {
    root: path.resolve('src'),
    modulesDirectories: ['node_modules'],
    alias: {
      scss: 'client/scss/landing'
    },
    extensions: ['', '.js', '.jsx', '.scss'],
    devtool: 'cheap-module-eval-source-map'
  },
  entry: [
    'webpack-hot-middleware/client',
    path.resolve(root, PATHS.landing)
  ],
  output: {
    path: path.resolve(root, PATHS.dist),
    filename: 'app.js',
    publicPath: '/dist/'
  }
}

exports.commonProd = {
  resolve: {
    root: path.resolve('src'),
    modulesDirectories: ['node_modules'],
    alias: {
      scss: 'client/scss/landing'
    },
    extensions: ['', '.js', '.jsx', '.scss']
  },
  entry: {
    landing: path.resolve(root, PATHS.landing),
    app: path.resolve(root, PATHS.app),
    style: path.resolve(root, PATHS.style),
    vendor: ['react', 'react-dom', 'react-router']
  },
  output: {
    path: path.resolve(root, PATHS.dist),
    filename: '[name].js',
    publicPath: '/dist/'
  }
}

exports.devServer = {
  devServer: {
    contentBase: path.resolve('./dist'),
    historyApiFallback: true,
    hot: true,
    inline: true,
    noInfo: true
  }
}

exports.loaderJS = {
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel',
        include: path.resolve(root, PATHS.js),
        query: {
          cacheDirectory: true,
          presets: ['react-hmre']
        }
      }
    ]
  }
}

exports.loaderJSON = {
  module: {
    loaders: [
      {
        test: /\.json?$/,
        loader: 'json-loader',
        include: path.resolve(root, PATHS.json),
      }
    ]
  }
}

exports.loaderCSS = {
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loaders: ['style', 'css', 'sass'],
        include: path.resolve(root, PATHS.scss)
      }
    ]
  }
}

exports.hotPlugin = {
  plugins: [
    new webpack.HotModuleReplacementPlugin({
      multiStep: true
    })
  ]
}

exports.minify = {
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ]
}

exports.setFreeVariable = function(key, value) {
  const env = {}
  env[key] = JSON.stringify(value);

  return {
    plugins: [
      new webpack.DefinePlugin(env)
    ]
  };
}

exports.commonsChunk = {
  plugins: [
    // Extract bundle and manifest files. Manifest is
    // needed for reliable caching.
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    })
  ]
}

exports.extractCSS = function(paths) {
  return {
    module: {
      loaders: [
        // Extract CSS during build
        {
          test: /\.scss$/,
          loader: ExtractTextPlugin.extract('style', 'css!sass'),
          include: path.resolve(root, PATHS.scss)
        }
      ]
    },
    plugins: [
      // Output extracted CSS to a file
      new ExtractTextPlugin('[name].css')
    ]
  };
}
