// Production

import React, { Component } from 'react'
import { render } from 'react-dom'

import * as lib from './lib/index'
import * as landingForm from './lib/landing-form'

import Simulator from './components/_simulator'
render(<Simulator />, document.querySelector('.Simulator'))


//Development

// import React, { Component } from 'react'
// import { render } from 'react-dom'
//
// module.hot.accept()
// require('scss')
//
// import Landing from './components/_landing.jsx'
// import ThankPage from './components/_thankpage.jsx'
// import Login from './components/_login.jsx'
// import Signup from './components/_signup.jsx'
//
// render(<Landing />, document.querySelector('#App'))
