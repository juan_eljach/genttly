from django.contrib import admin
from .models import Course, CourseClass

class CourseAdmin(admin.ModelAdmin):
		exclude = ("slug",)

class CourseClassAdmin(admin.ModelAdmin):
		exclude = ("slug",)

admin.site.register(Course, CourseAdmin)
admin.site.register(CourseClass, CourseClassAdmin)
# Register your models here.
