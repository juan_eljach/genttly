from django.conf import settings
from django.db import models
from django.template import defaultfilters
from hashlib import sha1
from courses.models import CourseClass

class Video(models.Model):
    title = models.CharField(max_length=150)
    url = models.URLField(blank=True)
    iframe = models.TextField()
    course_class = models.ForeignKey(CourseClass, related_name="videos")
    slug = models.SlugField(max_length=300)
    description = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = defaultfilters.slugify(self.title)
            while True:
            	try:
            		video_with_slug_exists = Video.objects.get(slug__iexact=self.slug)
            		if video_with_slug_exists:
            			self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
            	except Video.DoesNotExist: break
        super(Video, self).save(*args, **kwargs)

    def __str__(self):
    	return self.title

class VideoComment(models.Model):
    user_profile = models.ForeignKey(settings.AUTH_PROFILE_MODULE)
    comment = models.TextField(max_length=300)
    video = models.ForeignKey(Video, related_name="comments")
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-id' ,)

    def __str__(self):
        return self.video.title

class Answer(models.Model):
        user_profile = models.ForeignKey(settings.AUTH_PROFILE_MODULE)
        answer = models.TextField(max_length=300)
        video_comment = models.ForeignKey(VideoComment, related_name="answers")
        timestamp = models.DateTimeField(auto_now_add=True)

        def __str__(self):
                return self.answer[:80]