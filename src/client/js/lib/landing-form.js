$(function(){
  let $AboveFormButton = $('#LandingFormButton')
  let $inputs = $('.Above__form input')
  let $nameInput = $inputs[0]
  let $cellPhoneInput = $inputs[1]
  let $emailInput = $inputs[2]
  let $errorFormMsg = $('.Above__button p')
  const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  const $LearnFormButton = $('#LearnFormButton')
  const $inputsLearn = $('.Learn__form input')
  const $emailInputLearn = $inputsLearn[2]

  console.log($inputsLearn)

  function getCookie(name){
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }

  function sendData(dataRequest){
    $.ajax({
      method: 'POST',
      url: 'http://genttly.com/api/lead/create/',
      headers: {
        'X-CSRFToken': getCookie('csrftoken')
      },
      data: {name: dataRequest.name, phone: dataRequest.phone, email: dataRequest.email}
    })
      .done((d)=> {
        _.forEach($inputs, (v)=>{
          v.value = ''
        })
        _.forEach($inputsLearn, (v)=>{
          v.value = ''
        })
        $errorFormMsg.css({display: 'none'})
        window.location.replace("http://genttly.com/thankpage");
      })
      .fail((f)=> console.log(f))
  }


  $AboveFormButton.click(()=>{
    var data = {name: $inputs[0].value, phone: $inputs[1].value, email: $inputs[2].value }
    let inputFills = 0;

    _.forEach($inputs, (v)=>{
      if(v.value === '') {
        v.style.borderBottom = '1px solid #E11B4C'
        $errorFormMsg.css({display:'block'})
      }
      else if(v.value !== ''){
        inputFills += 1
      }
    })

    if(inputFills === 3){
      if(regexEmail.test($emailInput.value)){
        sendData(data)
      }
    }

  })

  _.forEach($inputs, (inp)=>{
    $('#' + inp.id).change(()=> {
      if(inp.value !== ''){
        inp.style.borderBottom = '.5px solid #fff'
      }
    })
  })


  $LearnFormButton.click(()=>{
    var data = {name: $inputsLearn[0].value, phone: $inputsLearn[1].value, email: $inputsLearn[2].value }
    console.log('me pinchastes')

    let inputFillsLearn = 0;

    _.forEach($inputsLearn, (v)=>{
      if(v.value === '') {
        v.style.borderBottom = '1px solid #E11B4C'
        // $errorFormMsg.css({display:'block'})
      }
      else if(v.value !== ''){
        inputFillsLearn += 1
      }
    })

    if(inputFillsLearn === 3){
      if(regexEmail.test($emailInputLearn.value)){
        sendData(data)
      }
    }

  })

  _.forEach($inputsLearn, (inp)=>{
    $('#' + inp.id).change(()=> {
      if(inp.value !== ''){
        inp.style.borderBottom = '.5px solid #e5e5e5'
      }
    })
  })


})
