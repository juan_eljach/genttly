$(function(){

  let $loginEmail = $('#loginEmail')
  let $loginUsername = $('#loginUsername')
  let $loginPassword = $('#loginPassword')
  let $loginButton = $('#loginButton')

  $loginButton.click(()=> {

    let isValid = $loginEmail.val() && $loginPassword.val() && $loginUsername.val()

    if(isValid){
      $.ajax({
      url: 'http://127.0.0.1:8000/rest-auth/login/',
      method: 'POST',
      data: {
        email: $loginEmail.val(),
        username: $loginUsername.val(),
        password: $loginPassword.val()
      }
    })
    .done((data) => {
      console.log(data)

      window.localStorage.setItem('key', data.key)
    })
    .fail((error)=> console.log(error))

    }

  })

})



