from django.conf.urls import url
from courses.views import CourseListAPIView, CourseRetrieveAPIView, CourseClassRetrieveAPIView, LeadCreateAPIView
from videos.views import VideoRetrieveAPIView, VideoCommentCreateAPIView

urlpatterns = [
        #URL API configuration for COURSES
        url(r'^lead/create/$', LeadCreateAPIView.as_view(), name='landing_lead_create'),
        url(r'^courses/$', CourseListAPIView.as_view(), name='course_list'),
        url(r'^courses/(?P<course_slug>[-\w]+)/$', CourseRetrieveAPIView.as_view(), name='course_detail'),
        url(r'^courses/(?P<course_slug>[-\w]+)/course-class/(?P<courseclass_slug>[-\w]+)$', CourseClassRetrieveAPIView.as_view(), name='courseclass_detail'),

        #URL API Configuration for VIDEOS
        url(r'^courses/(?P<course_slug>[-\w]+)/material/(?P<video_slug>[-\w]+)/$', VideoRetrieveAPIView.as_view(), name='video_detail'),
        url(r'^comment/create/$', VideoCommentCreateAPIView.as_view(), name='video_comment_create'),
]