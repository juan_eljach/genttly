from rest_framework import serializers
from .models import Course, CourseClass, Lead
from videos.serializers import VideoSerializer, VideoSmallSerializer

class CourseClassSerializer(serializers.ModelSerializer):
	videos = VideoSerializer(many=True, read_only=True)
	class Meta:
		model = CourseClass
		fields = (
			'name',
			'description',
			'slug',
			'videos',
		)

class CourseClassSmallSerializer(serializers.ModelSerializer):
	videos = VideoSmallSerializer(many=True, read_only=True)
	class Meta:
		model = CourseClass
		fields = (
			'name',
			'description',
			'slug',
			'videos',
		)

class CourseSerializer(serializers.ModelSerializer):
	classes = CourseClassSerializer(many=True, read_only=True)
	class Meta:
		model = Course
		fields = (
			'id',
			'name',
			'description',
			'image',
			'slug',
			'available',
			'course_type',
			'classes',
		)

class LeadSerializer(serializers.ModelSerializer):
	class Meta:
		model = Lead
		fields = (
			'name',
			'phone',
			'email'
		)