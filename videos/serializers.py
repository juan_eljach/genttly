from rest_framework import serializers
from .models import Video, VideoComment, Answer

class AnswerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Answer
		fields = (
			'answer',
			'video_comment',
		)

class VideoCommentSerializer(serializers.ModelSerializer):
	answers = AnswerSerializer(many=True, read_only=True)
	class Meta:
		model = VideoComment
		fields = (
			'user_profile',
			'comment',
			'video',
			'answers',
		)

class VideoSerializer(serializers.ModelSerializer):
	comments = VideoCommentSerializer(many=True)
	class Meta:
		model = Video
		fields = (
			'title',
			'iframe',
			'course_class',
			'slug',
			'description',
			'comments',
		)

class VideoSmallSerializer(serializers.ModelSerializer):
	class Meta:
		model = Video
		fields = (
			'title',
			'slug'
		)