import React, {Component} from 'react'
// import * as signupLib from '../lib/signup'

export default class Signup extends Component {
  constructor(props){
    super(props)
  }

  render(){
    return <div className="LoginView">
      <header className="HeaderLogin">
        <h1>Genttly</h1>
      </header>
      <section className="MainLogin">
        <div className="Login">
          <h5>Crea tu cuenta</h5>
          <div className="Login__form">
            <div className="Login__username">
              <label htmlFor="">Tu usuario</label>
              <input id="signupUsername" type="text" placeholder="Escribe tu usuario"/>
            </div>
            <div className="Login__email">
              <label htmlFor="">Tu email</label>
              <input id="signupEmail" type="text" placeholder="alambrito@gmail.com"/>
            </div>
            <div className="Login__password">
              <label htmlFor="">Tu contraseña</label>
              <input id="signupPasswordOne" type="password" placeholder="Escribe tu contraseña"/>
            </div>
            <div className="Login__password">
              <label htmlFor="">Escribe nuevamente tu contraseña</label>
              <input id="signupPasswordTwo" type="password" placeholder="Escribe tu contraseña"/>
            </div>
            <div className="Login__button">
              <button id="signupButton" className="waves-effect waves-light btn">REGISTRARME</button>
            </div>
          </div>
        </div>
      </section>
      <footer className="Footer">
        <div className="Footer__copyright">
          <p>Genttly. Todos los derechos reservados, 2017</p>
        </div>
        <div className="Footer__support"><p><a href="">Soporte:(+57) 313 838 2148</a></p></div>
      </footer>
    </div>

  }

}