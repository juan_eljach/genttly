from django.contrib import admin
from .models import Video, VideoComment, Answer

class VideoAdmin(admin.ModelAdmin):
	exclude = ("slug",)

admin.site.register(Video, VideoAdmin)
admin.site.register(VideoComment)
admin.site.register(Answer)