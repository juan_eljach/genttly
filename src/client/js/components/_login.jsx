
import React, {Component} from 'react'
// import * as loginLib from '../lib/login'

export default class Login extends Component {
  constructor(props){
    super(props)
  }

  render(){
    return <div className="LoginView">
      <header className="HeaderLogin">
        <h1>Genttly</h1>
      </header>
      <section className="MainLogin">
        <div className="Login">
          <div className="Login__icon">
            <img src="dist/images/Sign_in.svg" alt=""/>
          </div>
          <p>¿Aún no tienes una cuenta? <a href="">Regístrate</a></p>
          <div className="Login__form">
            <div className="Login__email">
              <label htmlFor="">Tu email</label>
              <input id="loginEmail" type="text" placeholder="alambrito@gmail.com"/>
            </div>
            <div className="Login__username">
              <label htmlFor="">Tu usuario</label>
              <input id="loginUsername" type="text" placeholder="Escribe tu usuario"/>
            </div>
            <div className="Login__password">
              <label htmlFor="">Tu contraseña</label>
              <input id="loginPassword" type="password" placeholder="Escribe tu contraseña"/>
            </div>
            <div className="Login__forget">
              <a href="#">¿Olvidaste tu contraseña?</a>
            </div>
            <div className="Login__button">
              <button id="loginButton" className="waves-effect waves-light btn">Inicia Sesión</button>
            </div>
          </div>
        </div>
      </section>
      <footer className="Footer">
        <div className="Footer__copyright">
          <p>Genttly. Todos los derechos reservados, 2016</p>
        </div>
        <div className="Footer__support"><p><a href="">Soporte:(+57) 313 838 2148</a></p></div>
      </footer>
    </div>

  }

}
